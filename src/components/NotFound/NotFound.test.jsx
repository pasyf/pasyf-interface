import React from 'react';
import {render} from '@testing-library/react';
import {NotFound} from './NotFound';

test('renders not found page', () => {
    const {getByText} = render(<NotFound />);
    const linkElement = getByText(/404 not found/i);
    expect(linkElement).toBeInTheDocument();
});
