import React from 'react';
import ReactDOM from 'react-dom';

import {App} from './App';

import {AuthProvider} from './hooks';
import history from './utils/history';

import {Provider} from 'react-redux';
import {store} from './store';

import * as serviceWorker from './serviceWorker';

import 'materialize-css/sass/materialize.scss';
import './index.scss';

const onRedirectCallback = app => {
    history.push(
        app && app.targetUrl
            ? app.targetUrl
            : window.location.pathname
    );
};

ReactDOM.render(
    <AuthProvider
        domain={process.env.REACT_APP_DOMAIN}
        client_id={process.env.REACT_APP_CLIENT_ID}
        redirect_uri={window.location.origin}
        onRedirectCallback={onRedirectCallback}>
        <Provider store={store}>
            <App />
        </Provider>
    </AuthProvider>,
    document.getElementById('root')
);

serviceWorker.unregister();
